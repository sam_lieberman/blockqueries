# README #

### What is this tool for? ###

The purpose of this tool is to query the Ethereum blockchain for data. In particular, you can enter a range of block numbers (either manually, or by specifying a length
and getting the latest blocks). You can see a list of all addresses in transactions contained within the queried blocks, along with how much eth they sent and received, and whether each one is a contract or not. It also displays the total amount of eth sent in the blocks.

### How do I build and run the app? ###

* Install the Angular Cli by typing in the command 'npm install -g @angular/cli' in a terminal.
* Download the source code, then navigate to the root folder in a terminal. Run 'npm i' and then 'ng build' to build, and 'ng serve' to run the app.
* Currently the ethers.js provider the tool uses connects to the Kovan testnet using my API keys from etherscan and infura.
* To run tests, simply navigate to the project folder and run 'npm t'.