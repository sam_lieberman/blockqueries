import { Component } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { from } from 'rxjs';
import { BlockRangeInfo } from './models/block-range-info';
import { BlockchainService } from './services/blockchain.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  displayedColumns = ['address', 'ethReceived', 'ethSent', 'contract'];
  transactionsReport: BlockRangeInfo = new BlockRangeInfo;
  twoInputForm: FormGroup;
  oneInputForm: FormGroup;
  showTwoInputs: boolean = false;

  constructor(private blockchainService: BlockchainService) {
    this.oneInputForm = new FormGroup({
      blocksBack: new FormControl('')
    });

    this.twoInputForm = new FormGroup({
      startingBlockNumber: new FormControl(''),
      endingBlockNumber: new FormControl('')
     }, this.endBlockHigherThanStartBlock());
  }

  onClickSubmitTwoInputs() {
    const info = from(this.blockchainService.getBlockRangeInfo(this.twoInputForm?.value.startingBlockNumber, this.twoInputForm?.value.endingBlockNumber));
    info.subscribe(x => {
      this.transactionsReport = x;
    });
  }

  onClickSubmitOneInput() {
    const info = from(this.blockchainService.getBlockRangeInfoForLatestBlocks(this.oneInputForm?.value.blocksBack));
    info.subscribe(x => {
      this.transactionsReport = x;
    });
  }

  endBlockHigherThanStartBlock(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      let start = control.get('startingBlockNumber');
      let end = control.get('endingBlockNumber');      
      
      let result = start?.value > end?.value;
      return result ? {endBlockHigherThanStartBlock: true} : null;
    };
  }

  showOptions(event:MatCheckboxChange): void {
    this.showTwoInputs = event.checked;
  }
}
