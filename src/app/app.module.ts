import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input'; 
import {MatButtonModule} from '@angular/material/button'; 
import {MatCheckboxModule} from '@angular/material/checkbox'; 
import { AppComponent } from './app.component';
import { BlockchainService } from './services/blockchain.service';
import { ProviderService } from './services/provider.service';
import { EthersWrapper } from './wrappers/ethers-wrapper';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule
  ],
  providers: [ProviderService, BlockchainService, EthersWrapper],
  bootstrap: [AppComponent]
})
export class AppModule { }
