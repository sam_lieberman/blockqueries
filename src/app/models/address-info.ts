export class AddressInfo {
    address: string = '';
    ethReceived: string = '0';
    ethSent: string = '0';
    contract: boolean = false;
}