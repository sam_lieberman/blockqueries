import { BigNumber, utils } from "ethers";
import { AddressInfo } from "./address-info";

export class BlockRangeInfo {
    totalEthTransferred: string = '';
    addressInfos: AddressInfo[] = [];    
}