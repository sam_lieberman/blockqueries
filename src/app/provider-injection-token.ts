import { InjectionToken } from '@angular/core';
import { getDefaultProvider, providers } from 'ethers';

export const PROVIDER = new InjectionToken<providers.BaseProvider>('Ethereum Provider', {
  providedIn: 'root',
  factory: () => getDefaultProvider('kovan', {
    etherscan: 'US5VCEPQAVS5GAPDZUS95TMKM8B6CQ6XYJ',
    infura: 'e97bf348abd64d3eb5caa834cc04027c'
  })
});