import { TestBed } from '@angular/core/testing';
import { BlockchainService } from './blockchain.service';
import { ProviderService } from './provider.service';
import { BlockWithTransactions, TransactionResponse } from "@ethersproject/abstract-provider";
import { BaseProvider } from "@ethersproject/providers";
import { BigNumber } from 'ethers';
import { EthersWrapper } from '../wrappers/ethers-wrapper';
import { BlockRangeInfo } from '../models/block-range-info';

describe('BlockchainService', () => {
  let service: BlockchainService;
  let providerServiceSpy: jasmine.SpyObj<ProviderService>;
  let ethersWrapperSpy: jasmine.SpyObj<EthersWrapper>;

  beforeEach(() => {
    providerServiceSpy = jasmine.createSpyObj('ProviderService', ['getBlocksWithTransactions', 'getProvider']);
    ethersWrapperSpy = jasmine.createSpyObj<EthersWrapper>("EthersWrapper", [
      'formatEther', 'getCode'
    ]);
    TestBed.configureTestingModule({
      providers: [
        {provide: ProviderService, useValue: providerServiceSpy},
        {provide: EthersWrapper, useValue: ethersWrapperSpy}
      ]
    });
    service = TestBed.inject(BlockchainService);
  }); 

  describe('getBlockRangeInfo', () => {
    it('should return block range info with total eth transferred and address info array', async () => {
      const expectedTotalEth = '4.5';
      const provider = <BaseProvider>{};
      const blocks = [
        {
          transactions: [
            {value: BigNumber.from(5), to: '0x1', from: '0x4'}, 
            {value: BigNumber.from(6), to: '0x2', from: '0x5'}, 
            {value: BigNumber.from(7), to: '0x3', from: '0x6'}
          ] as TransactionResponse[]
        }, 
        {
          transactions: [
            {value: BigNumber.from(8), to: '0x1', from: '0x4'}, 
            {value: BigNumber.from(9), to: '0x2', from: '0x5'}, 
            {value: BigNumber.from(10), to: '0x3', from: '0x6'}
          ]
        }        
      ] as BlockWithTransactions[];

      providerServiceSpy.getBlocksWithTransactions.and.resolveTo(blocks);
      providerServiceSpy.getProvider.and.returnValue(provider);

      ethersWrapperSpy.formatEther
        .withArgs(BigNumber.from(45)).and.returnValue(expectedTotalEth)
        .withArgs(BigNumber.from(13)).and.returnValue('1.3')
        .withArgs(BigNumber.from(15)).and.returnValue('1.5')
        .withArgs(BigNumber.from(17)).and.returnValue('1.7')
        .withArgs(BigNumber.from(0)).and.returnValue('0');

      ethersWrapperSpy.getCode
        .withArgs(provider, '0x1').and.resolveTo('0x')
        .withArgs(provider, '0x2').and.resolveTo('0xABC')
        .withArgs(provider, '0x3').and.resolveTo('0x')
        .withArgs(provider, '0x4').and.resolveTo('0xDEF')
        .withArgs(provider, '0x5').and.resolveTo('0x')
        .withArgs(provider, '0x6').and.resolveTo('0xGHI');

      const result = await service.getBlockRangeInfo(1, 2);
      
      expect(result).toBeInstanceOf(BlockRangeInfo);
      expect(result.totalEthTransferred).toBe(expectedTotalEth);

      expect(result.addressInfos.length).toBe(6);

      expect(result.addressInfos[0].address).toBe('0x1');
      expect(result.addressInfos[0].contract).toBeFalse();
      expect(result.addressInfos[0].ethReceived).toBe('1.3');
      expect(result.addressInfos[0].ethSent).toBe('0');

      expect(result.addressInfos[1].address).toBe('0x2');
      expect(result.addressInfos[1].contract).toBeTrue();
      expect(result.addressInfos[1].ethReceived).toBe('1.5');
      expect(result.addressInfos[1].ethSent).toBe('0');

      expect(result.addressInfos[2].address).toBe('0x3');
      expect(result.addressInfos[2].contract).toBeFalse();
      expect(result.addressInfos[2].ethReceived).toBe('1.7');
      expect(result.addressInfos[2].ethSent).toBe('0');

      expect(result.addressInfos[3].address).toBe('0x4');
      expect(result.addressInfos[3].contract).toBeTrue();
      expect(result.addressInfos[3].ethReceived).toBe('0');
      expect(result.addressInfos[3].ethSent).toBe('1.3');

      expect(result.addressInfos[4].address).toBe('0x5');
      expect(result.addressInfos[4].contract).toBeFalse();
      expect(result.addressInfos[4].ethReceived).toBe('0');
      expect(result.addressInfos[4].ethSent).toBe('1.5');

      expect(result.addressInfos[5].address).toBe('0x6');
      expect(result.addressInfos[5].contract).toBeTrue();
      expect(result.addressInfos[5].ethReceived).toBe('0');
      expect(result.addressInfos[5].ethSent).toBe('1.7');
    });
  });
});
