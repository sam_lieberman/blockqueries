import { Injectable } from "@angular/core";
import { BigNumber } from "ethers";
import { TransactionResponse } from "@ethersproject/abstract-provider";
import { BlockRangeInfo } from "./../models/block-range-info";
import { AddressInfo } from "./../models/address-info";
import { ProviderService } from "./../services/provider.service";
import { EthersWrapper } from "../wrappers/ethers-wrapper";
import * as _ from "lodash";

@Injectable({
  providedIn: "root",
})
export class BlockchainService {
  constructor(
    private providerService: ProviderService,
    private ethersWrapper: EthersWrapper
  ) {}

  public async getBlockRangeInfoForLatestBlocks(numberOfBlocks: number) {
    const currentBlock = await this.providerService.getCurrentBlockNumber();    
    return await this.getBlockRangeInfo(currentBlock - numberOfBlocks, currentBlock);
  }

  public async getBlockRangeInfo(startBlock: number, endBlock: number) {
    const blockInfo: BlockRangeInfo = new BlockRangeInfo();
    let blocks = await this.providerService.getBlocksWithTransactions(
        startBlock,
        endBlock
    );
    const transactions = _.flatMap(blocks, (b) => b.transactions);

    blockInfo.totalEthTransferred = this.getTotalValue(transactions);
    blockInfo.addressInfos = await this.getAddressInfos(transactions);
    return blockInfo;
  }

  private async getAddressInfos(transactions: TransactionResponse[]) : Promise<AddressInfo[]> {
    let recevingAddresses = transactions.map((t) => <string>t.to);
    let sendingAddresses = transactions.map((t) => <string>t.from);

    let addresses = _.uniq(recevingAddresses.concat(sendingAddresses));

    let transactionsByToAddress = _.groupBy(transactions, (t) => t.to);
    let transactionsByFromAddress = _.groupBy(transactions, (t) => t.from);
    
    let results = addresses.map(async (a) => ({
      address: a,
      ethReceived: this.getTotalValue(transactionsByToAddress[a] ?? []),
      ethSent: this.getTotalValue(transactionsByFromAddress[a] ?? []),
      contract: (await this.ethersWrapper.getCode(this.providerService.getProvider(), <string>a)) !== "0x",
    }));

    return await Promise.all(results);
  }

  private getTotalValue(transactions: TransactionResponse[]) {
    return this.ethersWrapper.formatEther(
      transactions
        .map((t) => t.value ?? BigNumber.from(0))
        .reduce((prev, current) => prev.add(current), BigNumber.from(0))
    );
  }  
}
