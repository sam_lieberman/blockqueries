import { TestBed } from "@angular/core/testing";
import { EthersWrapper } from "../wrappers/ethers-wrapper";
import { ProviderService } from "./provider.service";
import { BaseProvider } from "@ethersproject/providers";
import { BlockWithTransactions } from "@ethersproject/abstract-provider";
import { PROVIDER } from "../provider-injection-token";

describe("ProviderService", () => {
  let service: ProviderService;
  let ethersWrapperMock: jasmine.SpyObj<EthersWrapper>;
  let providerMock: jasmine.SpyObj<BaseProvider>;
  let blocks: BlockWithTransactions[];

  beforeEach(() => {
    ethersWrapperMock = jasmine.createSpyObj<EthersWrapper>("EthersWrapper", [
      "getBlockNumber",
    ]);
    providerMock = jasmine.createSpyObj<BaseProvider>("BaseProvider", [
      "getBlockWithTransactions",
    ]);

    blocks = [{ number: 120 }, { number: 121 }, { number: 122 }] as BlockWithTransactions[];
      for (let i = 0; i < blocks.length; i++) {
        providerMock.getBlockWithTransactions
          .withArgs(blocks[i].number)
          .and.resolveTo((blocks[i] as BlockWithTransactions));
      }

    TestBed.configureTestingModule({
      providers: [
        ProviderService,
        {provide: EthersWrapper, useValue: ethersWrapperMock},
        {provide: PROVIDER, useFactory: () => providerMock},
      ]
    });
    service = TestBed.inject(ProviderService);
  });

  describe("getBlocksWithTransactions", () => {
    it("should return array of blocks", async () => {
      const result = await service.getBlocksWithTransactions(120, 122);
      expect(result).toEqual(blocks);
    });
  });
});
