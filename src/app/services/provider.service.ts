import { Inject, Injectable } from '@angular/core';
import { providers } from 'ethers';
import { PROVIDER } from '../provider-injection-token';
import { EthersWrapper } from '../wrappers/ethers-wrapper';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {

  constructor(@Inject(PROVIDER) public provider: providers.BaseProvider, private ethersWrapper: EthersWrapper) {}

  public getProvider() {
    return this.provider;
  }

  public async getCurrentBlockNumber() {
    return await this.ethersWrapper.getBlockNumber(this.provider);
  }
  
  public async getBlocksWithTransactions(startBlock: number, endBlock: number) {
    let blocks = [];
    for (let i = startBlock; i <= endBlock; i++) {
      let block = await this.provider.getBlockWithTransactions(i);
      blocks.push(block);
    }
    return blocks;
  } 
}
