import { Injectable } from "@angular/core";
import { ethers, providers, utils } from "ethers";

@Injectable({
    providedIn: 'root'
  })
export class EthersWrapper {
    public formatEther(wei: ethers.BigNumberish) {
        return utils.formatEther(wei);
    }

    public async getCode(provider: providers.BaseProvider, address:string, blockTag?: providers.BlockTag) {
        return await provider.getCode(address, blockTag);
    }

    public async getBlockNumber(provider: providers.BaseProvider) {
        return await provider.getBlockNumber();
    }
}